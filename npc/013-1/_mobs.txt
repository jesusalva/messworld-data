// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 013-1: Woodland Hills mobs
013-1,0,0,0,0	monster	Pink Flower	1014,13,0,100
013-1,0,0,0,0	monster	Spiky Mushroom	1019,12,0,100
013-1,0,0,0,0	monster	Mouboo	1028,5,0,100
013-1,0,0,0,0	monster	Mauve Plant	1029,1,270,180
013-1,0,0,0,0	monster	Mauve Plant	1029,4,2700000,1800000
013-1,0,0,0,0	monster	Cobalt Plant	1030,1,270,180
013-1,0,0,0,0	monster	Cobalt Plant	1030,2,2700000,1800000
013-1,0,0,0,0	monster	Gamboge Plant	1031,1,270,180
013-1,0,0,0,0	monster	Gamboge Plant	1031,2,2700000,1800000
013-1,0,0,0,0	monster	Alizarin Plant	1032,1,270,180
013-1,0,0,0,0	monster	Alizarin Plant	1032,2,2700000,1800000
013-1,0,0,0,0	monster	Silkworm	1035,4,60000,30000
013-1,0,0,0,0	monster	Clover Patch	1037,2,0,1000
013-1,0,0,0,0	monster	Squirrel	1038,25,30,20
013-1,0,0,0,0	monster	Butterfly	1055,10,30,20
