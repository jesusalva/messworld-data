018-1,77,60,0	script	Mike	NPC113,{
    mes "[Mike]";
    mes "\"I need black stingers to make some medicine to cure my sister.\"";
    next;

    @dq_level = 40;
    @dq_cost = 16;
    @dq_count = 4;
    @dq_name$ = "BlackScorpionStinger";
    @dq_friendly_name$ = "black stingers";
    @dq_money = 2500;
    @dq_exp = 500;

    callfunc "DailyQuest";

    next;
    mes "\"Hopefully I'll have enough soon.\"";
    close;
}

// Buy en masse miner set aten by slimes and pays market price without bonus?
/*

Cronos - Boss Point Validation & Exchanger? Possibly in Keshlam Swamps?
*/

