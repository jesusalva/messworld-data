
031-2,29,28,0	script	Angela#house	NPC196,{
    .@q= getq(KaizeiQuest_Cindy);
    if (.@q > 4) goto L_Hello_Again;
    if (.@q == 4) goto L_Reward;

    mes "...";
    goto L_close;

L_Reward:
    mes "[Angela]";
    mes "\"Hello " + strcharinfo(0) + ", thank you again. I'm so glad Cindy is back home safe.\"";
    next;
    mes "\"As I told you, my husband is an adventurer. He is on one of his travels, so he couldn't rescue Cindy himself.\"";
    next;
    mes "\"I want to give you one of his treasures. Beside all the junk he brings, there are some very valuable things.\"";
    next;
    mes "\"This item is called the Rock Knife. When you wield it, you feel as robust as a rock.\"";

    getinventorylist;
    if (@inventorylist_count == 100)
        goto L_Full_Inv;
    getitem RockKnife, 1;
    setq(KaizeiQuest_Cindy, 5);

    next;
    mes "\"I hope this will be useful for you.\"";
    next;
    mes "\"I am so glad Cindy is safe. But there is still another problem. The Yetis took away all the white and yellow present boxes we wanted to bring to Santa!\"";
    next;
    mes "\"Usually, Yetis are very shy - I wonder why they did that. There is something strange going on.\"";
    next;
    mes "\"May I ask you for help again? I'll give you a small reward for every 3 boxes of one color you bring me.\"";
    goto L_close;

L_Full_Inv:
    mes "\"Oh, it seems you carry so much stuff - I will keep it for you until you can take it.\"";
    goto L_close;

L_Hello_Again:
    mes "[Angela]";
    mes "\"Hello! Good to see you again. Please warm yourself.\"";
    next;
    menu
        "I just wanted to say hello.",L_Hello,
        "I have some yellow present boxes.",L_Yellow,
        "I have some white present boxes.",L_White;

L_Yellow:
    @dq_level = 70;
    @dq_cost = 32;
    @dq_count = 3;
    @dq_name$ = "YellowPresentBox";
    @dq_friendly_name$ = "yellow present box";
    @dq_money = 5300;
    @dq_exp = 1300;

    callfunc "DailyQuest";
    next;
    if (@dq_return == 4)
        mes "\"Santa will be glad to have them back.\"";
    goto L_close;

L_White:
    @dq_level = 80;
    @dq_cost = 64;
    @dq_count = 3;
    @dq_name$ = "WhitePresentBox";
    @dq_friendly_name$ = "white present box";
    @dq_money = 10800;
    @dq_exp = 2800;

    callfunc "DailyQuest";
    next;
    if (@dq_return == 4)
        mesq l("You are a great help!");
    goto L_close;

L_close:
    @dq_level = 0;
    @dq_cost = 0;
    @dq_count = 0;
    @dq_name$ = "";
    @dq_friendly_name$ = "";
    @dq_money = 0;
    @dq_exp = 0;
    @dq_return = 0;
    close;

L_Hello:
    if (getvaultid() && get_nibble(##03_TMWQUEST, 0) <= 3) goto L_VaultQuest;
    closeclientdialog;
    close;

L_VaultQuest:
    mes "";
    mesn;
    mesq l("Actually, there is one thing bugging me during this whole ordeal.");
    next;
    mesn;
    mesq l("Yetis used to be very shy. Since the Great Quake, they became aggressive.");
    next;
    mesn;
    mesq l("...So why would they kidnap Cindy?? %%i");
    next;
    mesn;
    mesq l("I figured out, maybe one of the sages could help. Would you mind asking them?");
    next;
    select
        l("I'll be back later."),
        rif(get_nibble(##03_TMWQUEST, 0) == 0, l("Sure, I'll ask them.")),
        rif(get_nibble(##03_TMWQUEST, 0) == 3, l("[Tell her the truth]")),
        rif(get_nibble(##03_TMWQUEST, 0) == 3, l("[Make up an excuse]"));
    mes "";
    switch (@menu) {
    case 2:
        mesn;
        mesq l("Wonderful! Please be careful out there.");
        set_nibble(##03_TMWQUEST, 0, 1);
        break;
    case 3:
        mesc l("%s faints.", strnpcinfo(1));
        next;
        // FALL-THROUGH
    case 4:
        mesn;
        mesq l("Thanks for informing me. I'm sure Nikolai and the sages will do something about it, then.");
        next;
        mesn;
        mesq l("I'm afraid I don't have anything of value to reward you with, but some %s which my husband was saving for a quest or other. You can have them. Cindy is more important than a quest.", getitemlink(TreasureKey));
        set_nibble(##03_TMWQUEST, 0, 4);
        getitem TreasureKey, rand2(15, 25);
        break;
    }
    close;
}
