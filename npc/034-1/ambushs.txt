
034-1,118,59,0	script	#Ambush0Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush0_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush0_Spawn > 0)
        end;
    $@Ambush0VictimID = getcharid(3);
    donpcevent "#Ambush0::OnAmbush";
    end;
}

034-1,118,59,0	script	#Ambush0	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush0VictimID) == 0)
        goto L_Clean;
    $@Ambush0_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 115, 56, 121, 62, "", 1090, $@Ambush0_Spawn, "#Ambush0::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush0_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush0_Spawn = $@Ambush0_Spawn - 1;

    if (($@Ambush0VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush0_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush0VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush0_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,94,91,0	script	#Ambush1Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush1_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush1_Spawn > 0)
        end;
    $@Ambush1VictimID = getcharid(3);
    donpcevent "#Ambush1::OnAmbush";
    end;
}

034-1,94,91,0	script	#Ambush1	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush1VictimID) == 0)
        goto L_Clean;
    $@Ambush1_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 91, 88, 97, 94, "", 1090, $@Ambush1_Spawn, "#Ambush1::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush1_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush1_Spawn = $@Ambush1_Spawn - 1;

    if (($@Ambush1VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush1_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush1VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush1_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,122,35,0	script	#Ambush2Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush2_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush2_Spawn > 0)
        end;
    $@Ambush2VictimID = getcharid(3);
    donpcevent "#Ambush2::OnAmbush";
    end;
}

034-1,122,35,0	script	#Ambush2	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush2VictimID) == 0)
        goto L_Clean;
    $@Ambush2_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 119, 32, 125, 38, "", 1090, $@Ambush2_Spawn, "#Ambush2::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush2_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush2_Spawn = $@Ambush2_Spawn - 1;

    if (($@Ambush2VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush2_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush2VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush2_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,83,44,0	script	#Ambush3Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush3_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush3_Spawn > 0)
        end;
    $@Ambush3VictimID = getcharid(3);
    donpcevent "#Ambush3::OnAmbush";
    end;
}

034-1,83,44,0	script	#Ambush3	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush3VictimID) == 0)
        goto L_Clean;
    $@Ambush3_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 80, 41, 86, 47, "", 1090, $@Ambush3_Spawn, "#Ambush3::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush3_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush3_Spawn = $@Ambush3_Spawn - 1;

    if (($@Ambush3VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush3_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush3VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush3_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,74,28,0	script	#Ambush4Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush4_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush4_Spawn > 0)
        end;
    $@Ambush4VictimID = getcharid(3);
    donpcevent "#Ambush4::OnAmbush";
    end;
}

034-1,74,28,0	script	#Ambush4	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush4VictimID) == 0)
        goto L_Clean;
    $@Ambush4_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 71, 25, 77, 31, "", 1090, $@Ambush4_Spawn, "#Ambush4::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush4_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush4_Spawn = $@Ambush4_Spawn - 1;

    if (($@Ambush4VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush4_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush4VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush4_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,26,44,0	script	#Ambush5Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush5_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush5_Spawn > 0)
        end;
    $@Ambush5VictimID = getcharid(3);
    donpcevent "#Ambush5::OnAmbush";
    end;
}

034-1,26,44,0	script	#Ambush5	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush5VictimID) == 0)
        goto L_Clean;
    $@Ambush5_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 23, 41, 29, 47, "", 1090, $@Ambush5_Spawn, "#Ambush5::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush5_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush5_Spawn = $@Ambush5_Spawn - 1;

    if (($@Ambush5VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush5_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush5VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush5_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,50,47,0	script	#Ambush6Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush6_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush6_Spawn > 0)
        end;
    $@Ambush6VictimID = getcharid(3);
    donpcevent "#Ambush6::OnAmbush";
    end;
}

034-1,50,47,0	script	#Ambush6	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush6VictimID) == 0)
        goto L_Clean;
    $@Ambush6_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 47, 44, 53, 50, "", 1090, $@Ambush6_Spawn, "#Ambush6::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush6_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush6_Spawn = $@Ambush6_Spawn - 1;

    if (($@Ambush6VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush6_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush6VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush6_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,36,62,0	script	#Ambush7Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush7_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush7_Spawn > 0)
        end;
    $@Ambush7VictimID = getcharid(3);
    donpcevent "#Ambush7::OnAmbush";
    end;
}

034-1,36,62,0	script	#Ambush7	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush7VictimID) == 0)
        goto L_Clean;
    $@Ambush7_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 33, 59, 39, 65, "", 1090, $@Ambush7_Spawn, "#Ambush7::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush7_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush7_Spawn = $@Ambush7_Spawn - 1;

    if (($@Ambush7VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush7_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush7VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush7_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,26,95,0	script	#Ambush8Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush8_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush8_Spawn > 0)
        end;
    $@Ambush8VictimID = getcharid(3);
    donpcevent "#Ambush8::OnAmbush";
    end;
}

034-1,26,95,0	script	#Ambush8	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush8VictimID) == 0)
        goto L_Clean;
    $@Ambush8_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 23, 92, 29, 98, "", 1090, $@Ambush8_Spawn, "#Ambush8::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush8_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush8_Spawn = $@Ambush8_Spawn - 1;

    if (($@Ambush8VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush8_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush8VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush8_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,56,91,0	script	#Ambush9Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush9_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush9_Spawn > 0)
        end;
    $@Ambush9VictimID = getcharid(3);
    donpcevent "#Ambush9::OnAmbush";
    end;
}

034-1,56,91,0	script	#Ambush9	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush9VictimID) == 0)
        goto L_Clean;
    $@Ambush9_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 53, 88, 59, 94, "", 1090, $@Ambush9_Spawn, "#Ambush9::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush9_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush9_Spawn = $@Ambush9_Spawn - 1;

    if (($@Ambush9VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush9_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush9VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush9_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,65,66,0	script	#Ambush10Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush10_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush10_Spawn > 0)
        end;
    $@Ambush10VictimID = getcharid(3);
    donpcevent "#Ambush10::OnAmbush";
    end;
}

034-1,65,66,0	script	#Ambush10	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush10VictimID) == 0)
        goto L_Clean;
    $@Ambush10_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 62, 63, 68, 69, "", 1090, $@Ambush10_Spawn, "#Ambush10::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush10_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush10_Spawn = $@Ambush10_Spawn - 1;

    if (($@Ambush10VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush10_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush10VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush10_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,86,68,0	script	#Ambush11Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush11_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush11_Spawn > 0)
        end;
    $@Ambush11VictimID = getcharid(3);
    donpcevent "#Ambush11::OnAmbush";
    end;
}

034-1,86,68,0	script	#Ambush11	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush11VictimID) == 0)
        goto L_Clean;
    $@Ambush11_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 83, 65, 89, 71, "", 1090, $@Ambush11_Spawn, "#Ambush11::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush11_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush11_Spawn = $@Ambush11_Spawn - 1;

    if (($@Ambush11VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush11_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush11VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush11_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,101,79,0	script	#Ambush12Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush12_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush12_Spawn > 0)
        end;
    $@Ambush12VictimID = getcharid(3);
    donpcevent "#Ambush12::OnAmbush";
    end;
}

034-1,101,79,0	script	#Ambush12	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush12VictimID) == 0)
        goto L_Clean;
    $@Ambush12_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 98, 76, 104, 82, "", 1090, $@Ambush12_Spawn, "#Ambush12::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush12_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush12_Spawn = $@Ambush12_Spawn - 1;

    if (($@Ambush12VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush12_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush12VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush12_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,124,88,0	script	#Ambush13Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush13_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush13_Spawn > 0)
        end;
    $@Ambush13VictimID = getcharid(3);
    donpcevent "#Ambush13::OnAmbush";
    end;
}

034-1,124,88,0	script	#Ambush13	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush13VictimID) == 0)
        goto L_Clean;
    $@Ambush13_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 121, 85, 127, 91, "", 1090, $@Ambush13_Spawn, "#Ambush13::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush13_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush13_Spawn = $@Ambush13_Spawn - 1;

    if (($@Ambush13VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush13_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush13VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush13_Spawn <= 0)
        goto L_Clean;
    end;
}


034-1,117,103,0	script	#Ambush14Trigger	NPC32767,2,2,{
    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);
    if (@state != 4)
        end;

    if (@ambushs034 == $@Q_Barbarians_Ambush_max)
        @ambushs034 = 0;
    if (@ambushs034 & $@Q_Barbarians_Ambush14_flag)
        end;
    if (rand(50) < wolvern_count)
        end;

    if ($@Ambush14_Spawn > 0)
        end;
    $@Ambush14VictimID = getcharid(3);
    donpcevent "#Ambush14::OnAmbush";
    end;
}

034-1,117,103,0	script	#Ambush14	NPC32767,{
end;

OnAmbush:
    if (attachrid($@Ambush14VictimID) == 0)
        goto L_Clean;
    $@Ambush14_Spawn = 3 + rand(2);
    message strcharinfo(0), "An ambush!";
    areamonster "034-1", 114, 100, 120, 106, "", 1090, $@Ambush14_Spawn, "#Ambush14::OnWolvernDeath";
    @ambushs034 = @ambushs034 | $@Q_Barbarians_Ambush14_flag;
    end;

OnWolvernDeath:
    @mobId = 1090;

    @state = ((QUEST_Barbarians & $@Q_Barbarians_MASK) >> $@Q_Barbarians_SHIFT);

    $@Ambush14_Spawn = $@Ambush14_Spawn - 1;

    if (($@Ambush14VictimID == getcharid(3)) && (@state == 4))
        goto L_Count;
    if ($@Ambush14_Spawn > 0)
        end;
    goto L_Clean;

L_Clean:
    $@Ambush14VictimID = 0;
    end;

L_Count:
    wolvern_count = wolvern_count + 1;
    if (wolvern_count >= $@Q_Barbarians_wolvern_amount)
        message strcharinfo(0), "You've hunted down a lot of Wolverns. Maybe you should talk to Birrod?";
    if ($@Ambush14_Spawn <= 0)
        goto L_Clean;
    end;
}

