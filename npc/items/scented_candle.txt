-	script	ScentedCandle	NPC32767,{
    end;

OnEquip:
    if (@scented_candle == 1) end;
    goto L_Check;

OnCheck:
    if (getequipid(equip_head) != 5229) goto L_Removed;
    goto L_Check;

L_Check:
    @scented_candle = 1;
    getmapxy(.@m$, .@x, .@y, 0);
    .@r = 12;

    .@c=getunits(BL_MOB, .@mbs, false, .@m$, .@x-.@r, .@y-.@r, .@x+.@r, .@y+.@r);
    for (.@i = 0; .@i < .@c; .@i++) {
        @target_id=.@mbs[.@i];
        //if (target(BL_ID, @target_id, 0x20) != 0x20) end; // line of sight
        if (getunitdata(@target_id, UDT_CLASS) != 1131 && // mana bug
            getunitdata(@target_id, UDT_CLASS) != 1055 && // butterfly
            getunitdata(@target_id, UDT_CLASS) != 1049 && // bee
            getunitdata(@target_id, UDT_CLASS) != 1088) end; // hyvern
        aggravate @target_id;
    }
    addtimer rand(2000), strnpcinfo(0) + "::OnCheck";
    end;

OnMob:
    end;

L_Removed:
    @scented_candle = 0;
    end;
}
