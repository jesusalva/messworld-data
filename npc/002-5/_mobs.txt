// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 002-5: Deep Desert Mines mobs
002-5,37,89,6,3	monster	Angry Fire Goblin	1108,3,100000,30000
002-5,33,76,1,5	monster	Cave Maggot	1056,3,100000,30000
002-5,40,68,3,2	monster	Angry Scorpion	1057,3,100000,30000
002-5,40,38,9,2	monster	Archant	1060,3,100000,30000
002-5,56,97,1,2	monster	Archant	1060,1,100000,30000
002-5,68,100,6,1	monster	Yellow Slime	1007,4,100000,30000
002-5,91,80,0,2	monster	Archant	1060,1,100000,30000
002-5,89,94,0,2	monster	Archant	1060,1,100000,30000
002-5,74,78,0,2	monster	Archant	1060,2,100000,30000
002-5,94,72,1,2	monster	Archant	1060,1,100000,30000
002-5,90,61,3,1	monster	Yellow Slime	1007,1,100000,30000
002-5,74,35,0,2	monster	Archant	1060,1,100000,30000
002-5,84,33,0,2	monster	Archant	1060,1,100000,30000
002-5,84,46,0,2	monster	Archant	1060,1,100000,30000
002-5,99,38,0,2	monster	Archant	1060,1,100000,30000
002-5,93,38,4,1	monster	Yellow Slime	1007,2,100000,30000
002-5,62,62,2,21	monster	Angry Fire Goblin	1108,3,100000,30000
