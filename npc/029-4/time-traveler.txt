
029-1,25,40,0	script	Time Traveler#291	NPC184,{
    mes l("Would you like to time travel?");
    menu
        l("Yes!"), L_Warp,
        l("No"), -;
    closeclientdialog;
    close;

L_Warp:
    mes l("Hang tight!");
    close2;
    warp "029-4", 24, 38;
    close;
}

029-4,26,36,0	script	Time Traveler#294	NPC184,{
    mes l("Welcome back!");
    menu
        l("What year are we in?"), L_What,
        l("How do I leave?"),      L_Leave;

L_What:
    mes l("This is what Candor used to look like back in 2014.");
    next;
    mes l("I think this island is a bit volcanic, as after the Great Quake, more land emerged.");
    mes l("Settlers moved in shortly after.");
    close;

L_Leave:
    mes l("Walk West from here to escape the time dilation bubble.");
    close;
}
