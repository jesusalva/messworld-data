// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 011-1: Woodland mobs
011-1,0,0,0,0	monster	Evil Mushroom	1013,30,0,0
011-1,0,0,0,0	monster	Pink Flower	1014,20,0,0
011-1,0,0,0,0	monster	Spiky Mushroom	1019,5,0,0
011-1,0,0,0,0	monster	Snail	1041,1,0,0
011-1,0,0,0,0	monster	Mouboo	1028,5,0,10
011-1,0,0,0,0	monster	Mauve Plant	1029,3,270000,180000
011-1,0,0,0,0	monster	Alizarin Plant	1032,1,2700000,1800000
011-1,0,0,0,0	monster	Silkworm	1035,2,60000,30000
011-1,0,0,0,0	monster	Clover Patch	1037,2,0,1000
011-1,0,0,0,0	monster	Squirrel	1038,25,30,20
011-1,0,0,0,0	monster	Butterfly	1055,10,30,20
011-1,0,0,0,0	monster	Mouboo	1028,5,0,10
011-1,1,1,0,0	monster	Mana Bug	1131,18,30,0
011-1,92,44,17,14	monster	Maggot	1002,10,30,0
