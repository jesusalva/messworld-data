// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 043-4: Troll Cave mobs
043-4,139,163,3,3	monster	Terranite	1062,2,240000,120000
043-4,133,45,7,8	monster	Undead Troll	1117,4,100000,50000
043-4,91,139,5,5	monster	Undead Troll	1117,2,100000,50000
043-4,35,120,5,5	monster	Undead Troll	1117,2,100000,50000
043-4,43,143,6,5	monster	Undead Witch	1116,2,120000,60000
043-4,102,153,4,4	monster	Undead Witch	1116,2,120000,60000
043-4,142,159,7,6	monster	Undead Witch	1116,2,120000,60000
043-4,150,33,8,7	monster	Undead Witch	1116,1,120000,60000
043-4,103,54,4,11	monster	Undead Troll	1117,3,100000,50000
043-4,47,38,10,5	monster	Troll	1054,7,100000,50000
043-4,98,85,4,7	monster	Snake	1010,5,100000,50000
043-4,104,57,7,13	monster	Troll	1054,7,100000,50000
043-4,89,64,4,5	monster	Black Scorpion	1009,5,100000,50000
043-4,143,46,16,14	monster	Troll	1054,7,100000,50000
043-4,154,106,13,14	monster	Black Scorpion	1009,8,100000,50000
043-4,162,77,7,11	monster	Snake	1010,6,100000,50000
043-4,152,143,5,5	monster	Black Scorpion	1009,6,100000,50000
043-4,159,161,5,4	monster	Snake	1010,6,100000,50000
043-4,98,138,10,10	monster	Troll	1054,5,100000,50000
043-4,45,106,9,12	monster	Troll	1054,5,100000,50000
043-4,39,138,9,8	monster	Black Scorpion	1009,5,100000,50000
043-4,133,123,15,11	monster	Troll	1054,3,100000,50000
