// The Mana World Script
// (C) Jesusalva, 2021
// Licensed under GPLv2 or later

002-2,68,25,0	script	Bartender#Casino	NPC112,{
    shop .name$;
    goodbye;
    close;

OnInit:
    tradertype(NST_ZENY);
    sellitem Beer;
    sellitem Cake;
    sellitem CherryCake;
    .distance = 5;
    end;
}

