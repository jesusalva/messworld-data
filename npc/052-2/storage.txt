
052-2,33,79,0	script	#TriggerItemsInvoker	NPC32767,1,0,{
    @step = 6;
    if ($@illia_progress < @step) goto L_ShouldNotBeHere;
    if (strcharinfo(0) != $@iLLIA_HERO$) end;
    if ($@illia_level_6_progress == 0) goto L_StartItemInvoker;
    end;

L_ShouldNotBeHere:
    die();
    end;

L_StartItemInvoker:
    $@illia_level_6_progress = 1;
    donpcevent "#ItemsInvoker::OnCommandStart";
    end;

}

052-2,1,1,0	script	#ItemsInvoker	NPC32767,{
end;

OnInit:
    setarray $@illia_storage_items, 541, 567, 568, 562;
    setarray $@illia_storage_special_items, 687, 827, 1229;
    // Map coords:* x1,y1, x2,y2, ...
    setarray $@illia_storage_item_points, 30,77, 32,85, 37,81, 40,83, 44,86, 40,74, 44,75, 44,72, 31,71, 32,74;
    end;

OnCommandStart:
    $@illia_storage_max_items = 20;
    $@illia_storage_deviation = (8 + ($Illia_Luvia_Harvest*70)/100)*3;
    initnpctimer;
    areatimer 0, "052-2", 29, 70, 45, 88, 10, "#ItemsInvoker::OnStart";
    end;

OnStart:
    message strcharinfo(0), "You feel a strange atmosphere in this room... You sense a strong magic rising from the depth!";
    end;

OnTimer1000:
    if ($@illia_storage_max_items == 0)
        goto L_CheckMobs;
    $@item_invoke_index = rand(getarraysize($@illia_storage_item_points)/2);
    $@item_invoke_x = $@illia_storage_item_points[$@item_invoke_index*2];
    $@item_invoke_y = $@illia_storage_item_points[($@item_invoke_index*2)+1];
    npcwarp $@item_invoke_x, $@item_invoke_y, "#ItemsInvoker";
    // This is a fix to force the official mana client to display a npc after a warp.
    // Note: the manaplus client do not need this.
    disablenpc "#ItemsInvoker";
    end;

OnTimer1500:
    // See the note above.
    enablenpc "#ItemsInvoker";
    areatimer 0, "052-2", 29, 70, 45, 88, 10, "#ItemsInvoker::OnItem";
    misceffect FX_GROUND_SPAWN;
    end;

OnTimer3750:
    setnpctimer 0;
    $@item_invoke_x = 0;
    $@item_invoke_y = 0;
    $@item_invoke_index = 0;
    end;

OnItem:
    if (strcharinfo(0) == $@iLLIA_HERO$)
        goto L_ItemSpawn;
    end;

L_ItemSpawn:
    // This section will decide what to spawn: special monster, monster, item, special item
    // Calculation uses a decreasing variable which introduces a deviation.
    // Its initial value is based on $@illia_Luvia_Harvest
    //  * first decide (50% / 50%) if the deviation will be positive or negative
    //  * Apply the deviation to a random number between 0, 100. (note the final value can be < 0 or > 100)
    // The purpose is that, when the initial deviation is high, there are high chances to get either
    // a special monster, or a special item.
    @m = rand(100);
    if (@m < 50)
        @r = rand(100) - $@illia_storage_deviation;
    if (@m >= 50)
        @r = rand(100) + $@illia_storage_deviation;
    // Reduce the deviation
    $@illia_storage_deviation = $@illia_storage_deviation*83/100;
    if (@r < 5)
        goto L_MakeSpecialMonster;
    if (@r < 50)
        goto L_MakeMonster;
    if (@r > 98)
        goto L_MakeSpecialItem;
    // Normal item
    makeitem $@illia_storage_items[rand(getarraysize($@illia_storage_items))], rand(2, 8), "052-2", $@item_invoke_x, $@item_invoke_y;
    $@illia_storage_max_items = $@illia_storage_max_items - 1;
    @r = 0;
    end;

L_MakeSpecialMonster:
    monster "052-2", $@item_invoke_x, $@item_invoke_y, "", 1103, 1, "#ItemsInvoker::OnDeath";
    areatimer 0, "052-2", 29, 70, 45, 88, 10, "#ItemsInvoker::OnWtf";
    $@illia_storage_max_items = $@illia_storage_max_items - 1;
    end;

L_MakeMonster:
    monster "052-2", $@item_invoke_x, $@item_invoke_y, "", 1101, rand(1, 3), "#ItemsInvoker::OnDeath";
    $@illia_storage_max_items = $@illia_storage_max_items - 1;
    @r = 0;
    end;

L_MakeSpecialItem:
    makeitem $@illia_storage_special_items[rand(getarraysize($@illia_storage_special_items))], rand(2, 4), "052-2", $@item_invoke_x, $@item_invoke_y;
    areatimer 0, "052-2", 29, 70, 45, 88, 10, "#ItemsInvoker::OnWow";
    $@illia_storage_max_items = $@illia_storage_max_items - 1;
    @r = 0;
    end;

L_CheckMobs:
    if ($@illia_progress != 6)
        goto L_Clear;
    if (mobcount("052-2", "#ItemsInvoker::OnDeath") <= 0)
        goto L_Stop;
    setnpctimer 0;
    end;

L_Clear:
    stopnpctimer;
    setnpctimer 0;
    $@illia_storage_deviation = 0;
    $@illia_storage_max_items = 0;
    killmonster "052-2", "#ItemsInvoker::OnDeath";
    end;

L_Stop:
    stopnpctimer;
    setnpctimer 0;
    npcwarp 1, 1, "#ItemsInvoker";
    $@illia_storage_deviation = 0;
    $@illia_storage_max_items = 0;
    $@illia_level_6_progress = 2;
    $@illia_progress = 7;
    callfunc "UpdateIlliaProgress";
    $@illia_max_time = $@illia_max_time + 900;
    areatimer 0, "052-2", 29, 70, 45, 88, 10, "#ItemsInvoker::OnStop";
    end;

OnDeath:
    end;

OnStop:
    message strcharinfo(0), "The magic seems to quickly dissipate.";
    end;

OnWtf:
    message strcharinfo(0), "Look out!";
    end;

OnWow:
    message strcharinfo(0), "Wow, see what appeared!";
    end;

}

052-2,45,78,0	script	#ToPartyRoom	NPC32767,1,1,{
    @step = 6;
    if ($@illia_progress < @step) goto L_ShouldNotBeHere;
    if ($@illia_level_6_progress == 2) goto L_Warp;
    message strcharinfo(0), "The strong magic inside this room prevents you from going further.";
    end;

L_Warp:
    warp "052-2", 83, 58;
    end;

L_ShouldNotBeHere:
    die();
    end;

}
