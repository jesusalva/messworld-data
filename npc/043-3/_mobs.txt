// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 043-3: Sandy Dungeon Level 1 mobs
043-3,39,45,12,8	monster	Cave Maggot	1056,6,20000,20000
043-3,44,60,12,8	monster	Cave Maggot	1056,6,20000,20000
043-3,41,96,17,13	monster	Cave Maggot	1056,10,20000,20000
043-3,52,91,3,3	monster	Angry Scorpion	1057,5,30000,30000
043-3,102,116,13,9	monster	Angry Fire Goblin	1108,10,30000,30000
043-3,33,99,3,3	monster	Angry Scorpion	1057,5,30000,30000
043-3,116,78,12,9	monster	Angry Fire Goblin	1108,8,30000,30000
043-3,113,47,15,11	monster	Red Slime	1008,15,45000,35000
043-3,94,44,20,9	monster	Green Slime	1005,5,30000,15000
