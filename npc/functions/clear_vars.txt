
function	script	ClearVariables	{
    if (@login_event != 1) goto L_Deprecated;

    // Some temporary bugfix
    GM = getgroupid();

    // Remove old variables to new quest system
    if (QL_VALON) {
        setq CandorQuest_Valon, QL_VALON;
        QL_VALON = 0;
    }
    if (QL_HIDENSEEK) {
        setq CandorQuest_HideNSeek, (QL_HIDENSEEK >= 64 ? 2 : 1), (QL_HIDENSEEK-1);
        QL_HIDENSEEK = 0;
    }
    if (#CRYPT_PASSWORD && !getq(Quest_Reapercry)) {
        learnskill SKILL_REAPERCRY, 1;
        setq Quest_Reapercry, 1;
    }
    if (#DD5_TALLY) {
        setq Quest_Doomsday, 2;
        #DD5_TALLY = 0; // Whatever, one is enough
    }
    if (#BankAccount) {
        BankVault += #BankAccount;
        #BankAccount = 0;
    }
    if (QL_CINDY) {
        if (QL_CINDY == 5) QL_CINDY = 0;
        else if (QL_CINDY == 6) QL_CINDY = 1;
        else if (QL_CINDY == 1) QL_CINDY = 2;
        else if (QL_CINDY == 2) QL_CINDY = 3;
        else if (QL_CINDY == 3) QL_CINDY = 4;
        else if (QL_CINDY == 4) QL_CINDY = 5;
        setq KaizeiQuest_Cindy, QL_CINDY;
        QL_CINDY = 0;
    }
    if (MAGIC_EXPERIENCE) {
        setq2(MagicQuest_Healing, abs(MAGIC_EXPERIENCE >> 24));
        MAGIC_EXP = abs(MAGIC_EXPERIENCE & 65535);
        MAGIC_EXPERIENCE = 0;
        /* Convert the old magic system to new magic system */
        setq1(MagicQuest_DarkMage,
              OrumQuest);
        setq1(MagicQuest_Healing,
              (QUEST_MAGIC2 & NIBBLE_1_MASK) >> NIBBLE_1_SHIFT);
        //setq1(MagicQuest_Wyara,
        //      (QUEST_MAGIC2 & NIBBLE_2_MASK) >> NIBBLE_2_SHIFT);
        //setq1(MagicQuest_Kadiya,
        //      (QUEST_MAGIC2 & NIBBLE_3_MASK) >> NIBBLE_3_SHIFT);
        setq1(MagicQuest_Pauline,
              (QUEST_MAGIC2 & NIBBLE_4_MASK) >> NIBBLE_4_SHIFT);
        //setq1(MagicQuest_BrotherSword,
        //      (QUEST_MAGIC2 & NIBBLE_6_MASK) >> NIBBLE_6_SHIFT);
        //setq1(MagicQuest_SisterSword,
        //      (QUEST_MAGIC2 & (NIBBLE_6_MASK | NIBBLE_7_MASK)) >> NIBBLE_6_SHIFT); // Twobits, overlap w/ brothersword o.o
        //setq1(MagicQuest_Auldsbel,
        //      (QUEST_MAGIC & (NIBBLE_0_MASK | NIBBLE_1_MASK)) >> NIBBLE_0_SHIFT); // Twobits
        //setq1(MagicQuest_WhiteMage,
        //      (QUEST_MAGIC & NIBBLE_2_MASK) >> NIBBLE_2_SHIFT); // Boo + Druid
        //SAGATHA_ST = (QUEST_MAGIC & NIBBLE_3_MASK) >> NIBBLE_3_SHIFT; // Unhappy
        //Saggy - 4+5
        //Alchemist - 6+7 (War Quest - Swords again?)

        /* Update skill list */
        // Morgan & Orum
        if (QL_MORGAN >= 2)
            learnskill SKILL_CONFRINGO;
        if (OrumQuest >= 37)
            learnskill SKILL_HELORP;
        if (OrumQuest >= 38)
            learnskill SKILL_PHLEX;
        if (OrumQuest >= 41)
            learnskill SKILL_HALHISS;

        // Pauline
        if (getq(MagicQuest_Pauline) >= 3)
            learnskill SKILL_KALBOO;
        if (getq(MagicQuest_Pauline) >= 4)
            learnskill SKILL_KALGINA;

        // Auldsbel
        .@i=((QUEST_MAGIC & (NIBBLE_0_MASK | NIBBLE_1_MASK)) >> NIBBLE_0_SHIFT);
        if ((.@i >> 5) >= 5)
            learnskill SKILL_GOLE;
        .@i = .@i & 31;
        if (.@i >= 2)
            learnskill SKILL_PARUM;
        if (.@i >= 4)
            learnskill SKILL_PATVILOREE;
        if (.@i >= 5)
            learnskill SKILL_PATLOREE;
        if (.@i >= 6)
            learnskill SKILL_PATMUPLOO;
        if (.@i >= 7)
            learnskill SKILL_KULARZUFRILL;

        // War Mage
        .@i = (QUEST_MAGIC & NIBBLE_6_MASK) >> NIBBLE_6_SHIFT;
        if (.@i >= 1)
            learnskill SKILL_FLAR;
        if (.@i >= 2)
            learnskill SKILL_CHIZA;
        if (.@i == 4)
            learnskill SKILL_FRILLYAR;
        if (.@i == 5)
            learnskill SKILL_UPMARMU;
        if (.@i >= 6) {
            learnskill SKILL_FRILLYAR;
            learnskill SKILL_UPMARMU;
        }
        if (.@i >= 7)
            learnskill SKILL_INGRAV;

        // Healing magic
        if (getq(MagicQuest_Healing) >= 1)
            learnskill SKILL_LUM;
        if (getq(MagicQuest_Healing) >= 3)
            learnskill SKILL_INMA;

        // Light Mage
        .@i=((QUEST_MAGIC & (NIBBLE_4_MASK | NIBBLE_5_MASK)) >> NIBBLE_4_SHIFT);
        if (.@i >= 2)
            learnskill SKILL_CHIPCHIP;
        if (.@i >= 3)
            learnskill ALL_INCCARRY;
        if (.@i >= 4)
            learnskill SKILL_ASORM;
        if (.@i >= 5)
            learnskill SKILL_KALRENK;
        if (.@i >= 6)
            learnskill SKILL_KALAKARENK;
        if (.@i >= 7)
            learnskill SKILL_BETSANC;

        //QUEST_MAGIC  = 0;
        //QUEST_MAGIC2 = 0;
        OrumQuest = 0;
    }
    return;
}


function	script	ServerUpdate	{
    if ($@STARTUP) {
        debugmes "Cowardly refusing to update server outside startup";
        return;
    }

    // Correct Mana Pearl ID
    // qui jul 29 11:21:38 BRT 2021
    if ($VERSION < 1627568498) {
        ReplaceItemFromEveryPlayer(5270, 5272);
        ReplaceItemFromEveryPlayer(5267, 5273);
        ReplaceItemFromEveryPlayer(5268, 5274);
        ReplaceItemFromEveryPlayer(5269, 5275);
        $VERSION = 1627568498;
    }

    $@STARTUP = true;
    return;
}

