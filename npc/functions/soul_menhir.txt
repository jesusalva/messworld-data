function	script	SoulMenhir	{
    mesn l("Soul Menhir");
    mes l("(A mystical aura surrounds this stone. You feel mysteriously attracted to it. Something tells you to touch it. What do you do?)");

    select
        l("Touch it."),
        l("Leave it alone."),
        rif(!countitem(HitchhikersTowel), l("I lost my towel..."));
    mes "";
    if (@menu == 1) goto L_Bind;
    if (@menu == 3) goto L_Towel;
    return;

L_Towel:
    if (TowelLastUsed > (gettimetick(2) - 1800))
        goto L_DontPanic;
    TowelLastUsed = gettimetick(2);
    mesn l("Soul Menhir");
    mes "(You touch the mysterious stone. Somehow it feels hard and soft at the same time.)";
    getitembound HitchhikersTowel, 1, 4;
    goto L_Return;

L_Bind:
    if (Menhir_Activated == 1)
        goto L_Shortversion;

    mesn l("Soul Menhir");
    mes "(You touch the mysterious stone. Somehow it feels warm and cold at the same time.)";
    mes "(Suddenly a strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone.)";
    mes "(As suddenly as the feeling started it stops. The strange attraction is away from one moment to the next and the menhir feels like just an ordinary stone.)";
    Menhir_Activated = 1;
    goto L_Save;

L_Shortversion:
    mesn l("Soul Menhir");
    mes "(A strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone. As suddenly as the feeling started it stops.)";
    goto L_Save;

L_Save:
    if (@x == 0 && @y == 0)
        goto L_FindPoint;
    goto L_Do_Save;

L_DontPanic:
    message strcharinfo(0), "(A strange barrier keeps you from touching the stone at this time.)";
    goto L_Return;

L_Do_Save:
    savepoint @map$, @x, @y;
    goto L_Return;

L_FindPoint:
    @n = rand(getarraysize(@Xs));
    @x = @Xs[@n];
    @y = @Ys[@n];
    goto L_Do_Save;

L_Return:
    return;
}
