// The Mana World script
// Author: Jesusalva <jesusalva@themanaworld.org>
//
// Magic Script: SKILL_PHLEX (Level 1)
// School: Dark 2

function	script	SK_Phlex	{
    // Charge code, item check is in skill_db.conf (FIXME)
    mcharge(Root, SKILL_MAGIC_DARK, 1);
    .@PW=100+(10*@skillLv);
    // Effective magic code
    .@dmg=AdjustSpellpower(.@PW);
    harm(@skillTarget, .@dmg, HARM_MAGI, Ele_Dark);
    // May inflict poison
    .@sc=(getskilllv(SKILL_MAGIC_DARK) > 3 ? SC_DPOISON : SC_POISON);
    .@time=(1+.@dmg/33)*1000;
    .@cth=1+rand2(.@time)/15;
    sc_start .@sc, .@time, 1, .@cth, SCFLAG_NONE, @skillTarget;
    GetManaExp(@skillId, 2);
    return;
}


