// The Mana World script
// Author: Jesusalva <jesusalva@themanaworld.org>
//
// Magic Script: Betsanc and Asorm (Level 1)
// School: Nature/Astral 2

function	script	SK_Betsanc	{
    // party-guild filter
    if (!filter_sameguildorparty(@skillTarget)) {
        dispbottom b("Betsanc: ")+l("Skill can only be cast on party or guild members!");
        return;
    }
    // no GM Hat/Bots? Not needed due filter?
    delitem HardSpike, 1;
    // Same duration as Kaflosh
    .@PW=80+(20*@skillLv);
    .@dmg=AdjustSpellpower(.@PW);
    .@time=5+.@dmg/11;
    .@PX=10+cap_value(.@dmg/33, 0, 20);
    // SC, Time, DEF+, ASPD-
    sc_start2(SC_PHYSICAL_SHIELD, .@time*1000, .@PX*2, .@PX, 10000,
             SCFLAG_NOAVOID|SCFLAG_FIXEDTICK|SCFLAG_FIXEDRATE, @skillTarget);
    specialeffect FX_MAGIC_SHIELD_CAST, AREA, @skillTarget;
    GetManaExp(@skillId, 2);
    return;
}

function	script	SK_Asorm	{
    // party-guild filter
    if (!filter_sameguildorparty(@skillTarget)) {
        dispbottom b("Betsanc: ")+l("Skill can only be cast on party or guild members!");
        return;
    }
    // no GM Hat/Bots? Not needed due filter?
    delitem SmallMushroom, 1;
    // Same duration as Kaflosh
    .@PW=80+(20*@skillLv);
    .@dmg=AdjustSpellpower(.@PW);
    .@time=5+.@dmg/11;
    .@PX=10+cap_value(.@dmg/33, 0, 20);
    // SC_STONESKIN(??, def, mdef); Usually for mobs. Skill NPC_ANTIMAGIC
    // SC_FREYJASCROLL(MDEF, PerfectFlee)
    // SC_MDEFSET(MDEF)
    // SC, Time, MDEF+
    sc_start(SC_MDEFSET, .@time*1000, .@PX*2, 10000,
             SCFLAG_NOAVOID|SCFLAG_FIXEDTICK|SCFLAG_FIXEDRATE, @skillTarget);
    specialeffect FX_MAGIC_BARRIER_CAST, AREA, @skillTarget;
    GetManaExp(@skillId, 2);
    return;
}

