// The Mana World script
// Author: Jesusalva <jesusalva@themanaworld.org>
//
// Magic Script: SKILL_CHIZA (Level 1)
// School: War 1

function	script	SK_Chiza	{
    // Charge code, item check is here =( FIXME
    if (countitem(Dagger)) {
        mcharge(Dagger, SKILL_MAGIC_WAR, 8); .@PW=100;
    } else if (countitem(SharpKnife)) {
        mcharge(SharpKnife, SKILL_MAGIC_WAR, 6); .@PW=95;
    } else if (countitem(Knife)) {
        mcharge(Knife, SKILL_MAGIC_WAR, 4); .@PW=90;
    } else {
        dispbottom b("Chiza: ")+l("You need a Dagger, Sharp Knife or Knife to use!");
        return;
    }
    .@PW+=(10*@skillLv);
    // Effective magic code
    .@dmg=(AdjustSpellpower(.@PW)+AdjustAttackpower(.@PW))/2;
    harm(@skillTarget, .@dmg, HARM_PHYS, Ele_Neutral);
    GetManaExp(@skillId, 1);
    return;
}


