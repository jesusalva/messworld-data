
027-4,105,61,0	script	#KeshlamClue701	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the left handed singer";
    if (#CRYPT_PASSWORD & 1) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At south a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northeast a bright sparks.";
    close;
}

027-4,91,61,0	script	#KeshlamClue702	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the left handed lone warrior";
    if (#CRYPT_PASSWORD & 1) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southeast a bright sparks.";
    close;
}

027-4,99,61,0	script	#KeshlamClue703	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the left handed sailor";
    if (#CRYPT_PASSWORD & 1) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At south a bright sparks.";
    close;
}

027-4,121,61,0	script	#KeshlamClue704	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the left handed gargoyle";
    if (#CRYPT_PASSWORD & 2) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southeast a bright sparks.";
    close;
}



027-3,105,88,0	script	#KeshlamClue705	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 4) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At south a bright sparks.";
    close;
}

027-3,111,88,0	script	#KeshlamClue706	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 4) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northeast a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southwest a bright sparks.";
    close;
}




027-3,105,83,0	script	#KeshlamClue707	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 4) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At north a bright sparks.";
    close;

L_Off:
    mes "A reading says: At west a bright sparks.";
    close;
}

027-3,111,83,0	script	#KeshlamClue708	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 8) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At east a bright sparks.";
    close;

L_Off:
    mes "A reading says: At north a bright sparks.";
    close;
}



027-3,105,78,0	script	#KeshlamClue709	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 8) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southeast a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northwest a bright sparks.";
    close;
}

027-3,111,78,0	script	#KeshlamClue710	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 8) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northeast a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southwest a bright sparks.";
    close;
}



027-3,105,73,0	script	#KeshlamClue711	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 8) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At north a bright sparks.";
    close;

L_Off:
    mes "A reading says: At west a bright sparks.";
    close;
}

027-3,111,73,0	script	#KeshlamClue712	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 16) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At east a bright sparks.";
    close;

L_Off:
    mes "A reading says: At north a bright sparks.";
    close;
}



027-3,106,59,0	script	#KeshlamClue713	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 16) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southeast a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northwest a bright sparks.";
    close;
}

027-3,111,59,0	script	#KeshlamClue714	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 16) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At south a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northeast a bright sparks.";
    close;
}



027-3,106,54,0	script	#KeshlamClue715	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 32) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southeast a bright sparks.";
    close;
}

027-3,111,54,0	script	#KeshlamClue716	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 32) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At south a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northeast a bright sparks.";
    close;
}

027-4,111,61,0	script	#KeshlamClue750	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the right handed sailor";
    if (#CRYPT_PASSWORD & 64) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southeast a bright sparks.";
    close;
}

027-4,108,61,0	script	#KeshlamClue751	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the right handed duo";
    if (#CRYPT_PASSWORD & 64) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At south a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northeast a bright sparks.";
    close;
}

027-4,115,61,0	script	#KeshlamClue752	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the right handed Kage";
    if (#CRYPT_PASSWORD & 128) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At south a bright sparks.";
    close;
}

027-4,119,61,0	script	#KeshlamClue753	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "In memory of the right handed lone warrior";
    if (#CRYPT_PASSWORD & 128) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southeast a bright sparks.";
    close;
}



027-3,118,88,0	script	#KeshlamClue754	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 256) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At south a bright sparks.";
    close;
}

027-3,124,88,0	script	#KeshlamClue755	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 256) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northeast a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southwest a bright sparks.";
    close;
}



027-3,118,83,0	script	#KeshlamClue756	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 256) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At north a bright sparks.";
    close;

L_Off:
    mes "A reading says: At west a bright sparks.";
    close;
}

027-3,124,83,0	script	#KeshlamClue757	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 256) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At east a bright sparks.";
    close;

L_Off:
    mes "A reading says: At north a bright sparks.";
    close;
}



027-3,118,78,0	script	#KeshlamClue758	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 512) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At north a bright sparks.";
    close;

L_Off:
    mes "A reading says: At west a bright sparks.";
    close;
}

027-3,124,78,0	script	#KeshlamClue759	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 512) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At east a bright sparks.";
    close;

L_Off:
    mes "A reading says: At north a bright sparks.";
    close;
}



027-3,118,73,0	script	#KeshlamClue760	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 512) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At northeast a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southwest a bright sparks.";
    close;
}

027-3,124,73,0	script	#KeshlamClue761	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 512) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At south a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northeast a bright sparks.";
    close;
}



027-3,117,59,0	script	#KeshlamClue762	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 1024) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At south a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northeast a bright sparks.";
    close;
}

027-3,122,59,0	script	#KeshlamClue763	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 1024) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southeast a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northwest a bright sparks.";
    close;
}



027-3,117,54,0	script	#KeshlamClue764	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 2048) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At south a bright sparks.";
    close;

L_Off:
    mes "A reading says: At northeast a bright sparks.";
    close;
}

027-3,122,54,0	script	#KeshlamClue765	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    if (#CRYPT_PASSWORD & 2048) goto L_On;
    goto L_Off;

L_On:
    mes "A reading says: At southwest a bright sparks.";
    close;

L_Off:
    mes "A reading says: At southeast a bright sparks.";
    close;
}

027-5,68,94,0	script	#KeshlamClue791	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "There's an inscription on the gate.";
    next;
    mes "\"Krukan reads from bottom up. Four clues.\"";
    close;
}

027-5,68,87,0	script	#KeshlamClue792	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "There's an inscription on the gate.";
    next;
    mes "\"The candle shall lit you, but if you mess up, a different light will show.";
    mes "Alas did you knew, that Razha came before Krukan?\"";
    close;
}

027-5,68,73,0	script	#KeshlamClue793	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "There's an inscription on the gate.";
    next;
    mes "\"Razha likes to read from left to right. Two clues, Two clues.\"";
    close;
}

027-5,68,59,0	script	#KeshlamClue794	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "There's an inscription on the gate.";
    next;
    mes "\"The master's candlesticks are like the fingers of his hand.";
    mes "And if it is not to kill, they will never move.\"";
    close;
}

027-5,68,46,0	script	#KeshlamClue795	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "There's a bloody inscription on the gate.";
    next;
    mes "\"##B"+"reapercry"+"##b\"";
    learnskill SKILL_REAPERCRY, 1;
    next;
    mes "##9You hear the sounds of battle. Candle lights shines in distance.";
    next;
    mes "##9However, when you turn towards the source - both the light as the sound have already stopped.";
    next;
    mes "##9Is this a clue of some sort?";
    close;
}

099-7,95,64,0	script	Engraving#Keshlam	NPC400,{
    mes "There's an engraving on this tree.";
    next;
    if (countitem(FlawedLens) < 1) goto L_Tools;
    mes "Thanks to the lens on your inventory, you can read this:";
    mes "";
    mes "\"Nu'rem perished here, after the fight with Jande, Tal and Di'Tal.";
    mes "For this very reason, this land is plagued forever. We shall give Nu'Rem a proper burrial.\"";
    next;
    mes "-- The Sparron";
    close;

L_Tools:
    mes "It is too faint to read without some good lens, but you can make the following words out of the engraving:";
    next;
    mes "\"Nu'rem .... here .... fight .... Jande ....";
    mes ".... land .... plagued forever. .... Nu'Rem .... proper burrial.\"";
    next;
    mes "-- The ....";
    close;

OnInit:
    // In theory, pattern ID must be between 1~9
    // To set $@p1$~$@p9$ with the PERL expression
    // But as we're only using $@p0$ (full string)
    // the value itself is meaningless
    .pid=getnpcid();
    debugmes "Engraving: Pattern %d", .pid;
    //I'm not going to learn PERL just for that
    defpattern(.pid, "^(.*)$", "OnTalkNearby");
    activatepset(.pid);
    end;

OnTalkNearby:
    // Quest not assigned
    if (!getq(Quest_Doomsday)) end;

    // not very obvious stuff by gumi, $@p0$ contains the whole string
    // so we must cut it. Could use $@p1$ if perl was proper but... meh.
    .@no_nick$ = strip(substr($@p0$, getstrlen(strcharinfo(PC_NAME)) + 3, getstrlen($@p0$) - 1));
    .@message$ = strtoupper(.@no_nick$);

    // Only react if the message is what we want to hear
    if (.@message$ == "TUTIN JANDE LIAISE" || .@message$ == "ALONSIALONSO") {
        specialeffect(FX_MAGIC_TELEPORT, AREA, getcharid(3));
        sleep2(800);
        if (!ispcdead())
            warp "099-1", 55, 49;
    }
    end;
}


-	script	Keshlam	NPC32767,{
    close;

OnInit:
    areamonster "099-7", 40, 35, 140, 135, "", 1148, 10, "Keshlam::OnDeath8";
    areamonster "099-7", 40, 35, 140, 135, "", 1149, 10, "Keshlam::OnDeath9";
    monster "099-7", 40, 120, "Grim Reaper", 1068, 1, "Keshlam::OnBoss";
    end;

OnDeath8:
OnDeath9:
OnDeath:
    // TODO: Only fire this if timer not running. Set timer to 5min
    initnpctimer;
    end;

OnBoss:
    initnpctimer;
    if (playerattached()) {
        // If a player is attached, give them 20 Treasure Keys
        getitem TreasureKey, 20;
        BOSS_POINTS+=10;
        if (getq(Quest_Reapercry) < 3) {
            dispbottom l("Somehow, it does not feel *right* - Maybe you need to perform some sort of ritual before defeating the Grim Reaper?");
        } else if (getq(Quest_Reapercry) == 3) {
            getitembound UnderworldKey, 1, 4;
            setq Quest_Reapercry, 4;
            dispbottom l("The Reaper drops a key - It might be valuable, so you keep it.");
        } else {
            dispbottom l("Somehow, it does not feel *right* - We already defeated the Grim Reaper before anyway, didn't we?");
        }
    }
    end;

OnTimer150000:
    // 1148
    .@KeshlamMc = mobcount("099-7", "Keshlam::OnDeath8");
    if (.@KeshlamMc < 10)
        areamonster "099-7", 40, 35, 140, 135, "", 1148, 10-.@KeshlamMc, "Keshlam::OnDeath8";

    // 1149
    .@KeshlamMc = mobcount("099-7", "Keshlam::OnDeath9");
    if (.@KeshlamMc < 10)
        areamonster "099-7", 40, 35, 140, 135, "", 1149, 10-.@KeshlamMc, "Keshlam::OnDeath9";

    // Boss (TODO maybe give boss their own timer with 5 minutes)
    .@KeshlamMc = mobcount("099-7", "Keshlam::OnBoss");
    if (.@KeshlamMc < 1)
        monster "099-7", 40, 120, "Grim Reaper", Reaper, 1, "Keshlam::OnBoss";

    // Done
    .@KeshlamMc = 0;
    stopnpctimer;
    end;
}

099-7,38,125,0	script	Chest#keshlam	NPC111,{
    if (FLAGS & FLAG_KESHLAM_RAREDROP) goto L_Finished;
    mes "[Chest]";
    mes l("Would you try to open it?");
    mesc l("Cost: %s %ss", fnum(1000), getitemlink(TreasureKey)), 1;
    next;
    menu
        l("Yes."), L_Yes,
        l("No."),  L_close;

L_Yes:
    if (ispcdead())
        goto L_Not_Enough;
    if(countitem(TreasureKey) < 1000)
        goto L_Not_Enough;
    getinventorylist;
    if (@inventorylist_count == 100
        && countitem(TreasureKey) > 1000)
            goto L_TooMany;
    delitem TreasureKey, 1000;
    if (FLAGS & FLAG_KESHLAM_FLAWEDLENS) goto L_Rare;
    goto L_FlawedLens;

L_FlawedLens:
    getitem FlawedLens, 1;
    getexp 20000000, 0;
    FLAGS = FLAGS | FLAG_KESHLAM_FLAWEDLENS;
    mes "[Chest]";
    mes l("You opened it and found...! Some flawed lens?! Meh.");
    next;
    mes "[Chest]";
    mes l("You notice this chest has a hidden section with another lock.");
    mes l("You might want try that again later.");
    close;

L_Rare:
    getexp 10000000, 0;
    FLAGS = FLAGS | FLAG_KESHLAM_RAREDROP;
    if (rand2(4) == 1) goto L_Amulet; // 25% chance
    goto L_Ring; // 75% chance

L_Amulet:
    getitem EnchantersAmulet, 1;
    mes "[Chest]";
    mes l("You opened it and found...! An Enchanter's Amulet! Lucky!");
    close;

L_Ring:
    getitem MageRing, 1;
    mes "[Chest]";
    mes l("You opened it and found...! A Mage Ring! Lucky!");
    close;

L_Not_Enough:
    mes l("It seems that this is not the right key...");
    close;

L_Finished:
    mes "[Chest]";
    mes l("You already opened this chest.");
    close;

L_TooMany:
    mes "[Chest]";
    mes l("You don't have room for what ever is inside. Maybe you should try again later.");
    close;

L_close:
    close;
}

099-7,115,132,0	script	Knowledgeable Tree	NPC400,{
    mes l("If you kill every monster, this tree can warp you back.");
    mes l("You will receive experience points for cleaning the map.");
    mes "";
    mes l("But be warned, monsters respawn on their own every five minutes.");
    mes l("The boss must also be defeated.");
    next;
    @KeshlamMc = mobcount("099-7", "Keshlam::OnDeath8");
    @KeshlamMc = @KeshlamMc+mobcount("099-7", "Keshlam::OnDeath9");
    @KeshlamMc = @KeshlamMc+mobcount("099-7", "Keshlam::OnBoss");
    if (@KeshlamMc <= 0) goto L_Done;
    mes "There is ##B"+@KeshlamMc+" monsters##b alive.";
    close;

L_Done:
    mes l("Go back?");
    if (askyesno() == ASK_NO)
        close;
    mes "";
    getexp 1000000, 0;
    warp "027-2", 50, 29;
    close;
}

027-4,107,61,0	script	#NoobJesusalva01	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "(The memorial is at right, behind the column.)";
    close;
}
027-4,109,61,0	script	#NoobJesusalva02	NPC400,{
    if (!#CRYPT_PASSWORD) end;
    mes "(The memorial is at left, behind the column.)";
    close;
}
099-7,93,36,0	script	#NoobKytty01	NPC400,0,0,{
    slide 94, 33;
    end;
}
099-7,93,33,0	script	#NoobKytty02	NPC400,0,0,{
    slide 92, 36;
    end;
}

099-7,101,30,0	script	An Evil Obelisk	NPC185,{
    mes l("An evil obelisk. Totally not suspcious. At all.");
    // 1 = Assigned; 2 = Warped; 3 = Obelisk
    if (getq(Quest_Reapercry) == 2) {
        next;
        // FIXME: Infuse a Soul in the Broken Doll obtained from Fey Sprite
        // Then sacrifice it, opening the portal. [HOCUSIDEA]
        mesc l("(You touch the mysterious obelisk. Somehow you feels peace and pain at the same time.)");
        mesc l("(Suddenly a strange sensation flows through you. It feels like your body leaves your soul and becomes one with the stone.)");
        mesc l("(As suddenly as the feeling started it stops. The strange attraction is away from one moment to the next and the obelisk feels like just an ordinary evil stone.)");
        setq Quest_Reapercry, 3;
    }
    close;

OnInit:
    .distance=3;
    end;
}

