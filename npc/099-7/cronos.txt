// Cronos, the lord of time and apparently a shopkeeper
// There's also Father Time in the GM stuff

099-7,99,30,0	script	Chronos	NPC313,{
    mesn;
    mesq l("I'm Chronos, lord of time, the immortal, the head editor of the Almanach, the... Alright, let's forget that.");
    if (BOSS_POINTS < .price)
        close;
    // Boss Points validation
    .@cur=(BOSS_POINTS / .price);
    .@dif=.@cur - BOSS_MEDALS;
    if (.@dif) {
        next;
        getitembound BossMedal, .@dif, IBT_ACCOUNT;
        BOSS_MEDALS+=.@dif;
        mesn;
        mesq l("For your deeds, I shall bestow upon you %d %s. Use them wisely, they are the results of your hard effort.", .@dif, getitemlink(BossMedal));
        mesc l("Boss Medals are earned each %s boss points. They are account-bound and cannot be given to other players.", fnum(.price));
        next;
    }

    // Shopkeeping
    mesc l("All things shall pass, but with the limited time you have, please enjoy shopkeeping.");
    if (countitem(UnderworldKey)) {
        select
            l("Thanks."),
            l("Do you know what the Underworld Key is for?");
        mes "";
        if (@menu == 2) {
            mesn;
            mesq l("Yes, I do. But now is not the time. One day, when the event horizon aligns itself and the lazy titans move, then I shall tell you what you need to know."); // lazy titans = The developers and shout out to Kronus from mythology
            close;
        }
    } else {
        next;
    }
    openshop;
    closeclientdialog;
    close;

OnInit:
	tradertype(NST_CUSTOM);
    .distance=4;
    .price=10000;

    sellitem Goggles, 1;
    sellitem LeatherGoggles, 1;
    sellitem Eyepatch, 1;
    sellitem TopHat, 1;
    sellitem FunkyHat, 1;
    sellitem MushHat, 1;
    sellitem ShroomHat, 1;
    sellitem ChristmasElfHat, 1;
    sellitem FaceMask, 1;
    sellitem Crown, 1;
    sellitem Cap, 1;

    sellitem DesertHelmet, 1;
    sellitem CaptainsHat, 1;
    sellitem GuyFawkesMask, 1;
    sellitem WitchDoctorsMask, 1;
    sellitem Earmuffs, 1;
    sellitem ElfNightcap, 1;
    sellitem Sunglasses, 1;
    sellitem TamOShanter, 1;
    sellitem ChristmasTreeHat, 1;
    sellitem SantaBeardHat, 1;
    sellitem BunnyEars, 1;
    sellitem MoubooHead, 1;
    sellitem PaperBag, 1;
    sellitem BunchOfParsley, 1;
    sellitem SkullMask, 1;

    sellitem SnowGoggles, 1;
    sellitem HeartGlasses, 1;
    sellitem ReadingGlasses, 1;
    sellitem OperaMask, 1;
    sellitem JesterMask, 1;
    sellitem WitchHat, 1;
    sellitem GoblinMask, 1;

    sellitem BlinkingEvil, 1;
    sellitem BlinkingEvilRed, 1;
    sellitem BlinkingEvilBlue, 1;
    sellitem BlinkingEvilPink, 1;
    sellitem BlinkingEvilYellow, 1;
    sellitem BlinkingHocus, 1;

    sellitem PanHat, 1;
    sellitem ChefHat, 1;
    sellitem PilotHat, 1;
    sellitem MoonshroomHat, 1;
    sellitem CarbonGasMask, 1;
    sellitem EskimoHat, 1;
    sellitem GroovyHat, 1;
    sellitem JazzyHat, 1;
    sellitem ChicSantaHat, 1;
    sellitem AnniversaryHat, 1;
    sellitem AFKCap, 1;
    sellitem SmileyCap, 1;

    sellitem RedShades, 1;
    sellitem GreenShades, 1;
    sellitem DarkBlueShades, 1;
    sellitem YellowShades, 1;
    sellitem LightBlueShades, 1;
    sellitem PinkShades, 1;
    sellitem BlackShades, 1;
    sellitem OrangeShades, 1;
    sellitem PurpleShades, 1;
    sellitem DarkGreenShades, 1;

    sellitem MovieCap, 1;
    sellitem BlueWolfHelmet, 1;
    sellitem CloverHat, 1;
    sellitem RabbitEars, 1;
    sellitem EggshellHat, 1;
    sellitem Pipe, 1;

    sellitem WhiteEvokersRobeBlue, 1;
    sellitem BlackEvokersRobeBlue, 1;
    sellitem ApprenticeRobe, 1;
    sellitem GoldenWarlordPlate, 1;
    sellitem GoldenPlatemail, 1;

    sellitem ScarabArmlet, 1;
    sellitem SnowLauncher, 1;

    sellitem OverlordsHelmet, 2;
    sellitem DarkTalisman, 2;
    sellitem Aureole, 2;
    sellitem MageRing, 2;
    sellitem PaladinsHelmet, 2;
    sellitem EnchantersAmulet, 2;
    sellitem HeartOfIsis, 2;
    sellitem BansheeBow, 2;
    sellitem BullHelmet, 2;
    sellitem AssassinShirt, 2;
    sellitem LazuriteRobe, 2;
    sellitem DarkHelm, 2;
    sellitem UnderworldMask, 2;
    sellitem Phylactery, 2;
    end;

OnCountFunds:
	setcurrency(countitem(BossMedal));
	end;

OnPayFunds:
	if( countitem(BossMedal) < @price )
		end;
	delitem BossMedal, @price;
	purchaseok();
	end;
}

